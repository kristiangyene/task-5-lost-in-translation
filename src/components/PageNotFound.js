import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () => (
    <div>
        <h2>Page not found</h2>
        <Link to='/login'>Back to login</Link>
    </div>
);

export default PageNotFound;