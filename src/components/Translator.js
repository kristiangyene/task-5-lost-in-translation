import React, { useState, useEffect } from 'react';
import './Translator.css';
import { getStorage, setStorage } from '../utils/storage';
import { Link } from 'react-router-dom';


function Translator() {
    const [ name, setName ] = useState('');
    const [ translate, setTranslate ] = useState("");
    const [ outputTranslate, setOutputTranslate ] = useState([]);

    useEffect(() => {
        setName(getStorage('user'));
    }, []);

      const importAll = (r) => {
        let images = {};
        const imageUrls = r.keys().map(r);
        r.keys().forEach((key, index) => {
        key = key.replace(/^.*[\\/]/, '').split('.')[0];
        images[key] = imageUrls[index];
        });
        return images;
      }

      const images = importAll(require.context('../LostInTranslation_Resources/individial_signs', false, /\.(png|jpe?g|svg)$/));
      
      const onTranslateChange = (input) => setTranslate(input.target.value.trim().toLowerCase());

      const onTranslateClick = () => {
        const userTranslate = Array.from(translate);
        let outputTranslate = [];

        userTranslate.forEach((letter) => {
            if(!/[a-zA-Z]/g.test(letter)){
                outputTranslate.push(letter);
            }
            else outputTranslate.push(images[letter]);
        });
        setStorage(translate, outputTranslate);
        setOutputTranslate(outputTranslate);
    }

    const imageList = outputTranslate.map((image, index) => {
        if(!/[a-zA-Z]/g.test(image)){
            return(
                <span className="no-img" key={index}>{image}</span>
            );
        }
        else return (
            <img className="img" width="5px" src={ image } alt={ index } key={ index }/>
        );
      });

  return (
    <div className='Translator'>
        <div className="bar">
        <p className="current-page">{name}</p>
            <Link to="/Profile" className="link">Profile</Link>
        </div>
        <form className="form">
            <input className='input' 
                    type='text' 
                    placeholder='Text' 
                    minLength="1" 
                    maxLength="40" 
                    required pattern="[a-zA-Z]*"
                    onChange={onTranslateChange}/>
            <button className='button' type="button" onClick={onTranslateClick}>Translate</button>
            <div className="output">
                {imageList}
            </div>
        </form>
    </div>
  );
}

export default Translator;
