import React, { useState, useEffect } from 'react';
import './Login.css';
import { getStorage, setStorage } from '../utils/storage';
import { Link, Redirect } from 'react-router-dom';

function Login() {
    const [ isLoggedIn, setIsLoggedIn ] = useState(false);
    const [ name, setName ] = useState('');

    useEffect(() => {
        setIsLoggedIn(getStorage('user'));
    }, []);

    const onNameChange = (input) => setName(input.target.value.trim());
    const onLoginClicked = () => {
        setStorage('user', name);
        setIsLoggedIn(true);
    }
    
  return (
    <div className='Login'>
        {isLoggedIn && <Redirect to="/translator"/>}
        <h2>Login</h2>
        <input className='input' type='text' placeholder='name' onChange={onNameChange}></input>
        <Link to='/translator'>
        <button type='button' className='button' onClick={onLoginClicked}>GO</button>
        </Link>
    </div>
  );
}

export default Login;
