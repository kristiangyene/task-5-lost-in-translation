import React, { useState, useEffect } from 'react';
import './Profile.css';
import { getStorage } from '../utils/storage';
import { Link } from 'react-router-dom';


function Profile() {
    const [ name, setName ] = useState('');

    useEffect(() => {
        setName(getStorage('user'));
    }, []);

    const onLogoutClicked = () => localStorage.clear();

    const storageList = [];

    for(let i = 0; i < localStorage.length; i++){
        let translate = localStorage.key(i);
        if (translate !== 'user'){
            let outputTranslate = getStorage(translate);
            storageList.push({
                translate,
                outputTranslate
            });
        }
    }

    
    const translationList = storageList.map((translation, index) => {
        const imageList = translation.outputTranslate.map((image, index) => {
            if(!/[a-zA-Z]/g.test(image)){
                return(
                    <span key={index}>{image}</span>
                );
            }
            else return (
                <img className="img" width="3px" src={ image } alt={ index } key={ index }/>
            );
        });
        return (
        <tr key={index}>
            <td>{translation.translate}</td>
            <td>{imageList}</td>
        </tr>
        );
      });
    

  return (
    <div className='Profile'>
        <div className="bar">
            <p className="current-page">{name}</p>
            <Link to="/translator" className="link">Back</Link>
        </div>
        <h2>Profile</h2>
        <table className='table'>
            <thead className='table-element'>
                <tr className='tr'>
                    <th>Input</th>
                    <th>Translation</th>
                </tr>
            </thead>
            <tbody>
                {translationList}
            </tbody>
        </table>
        <Link to='/login'>
            <button type='button' className='button' onClick={onLogoutClicked}>Logout</button>
        </Link>
        <p>NB! Stored data will be deleted when logging out.</p>
    </div>
  );
}

export default Profile;
