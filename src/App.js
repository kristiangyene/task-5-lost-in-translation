import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Login from './components/Login';
import Translator from './components/Translator';
import Profile from './components/Profile';
import PageNotFound from './components/PageNotFound';

function App() {
  return (
    <Router>
      <div className='App'>
      <header className='App-header' id="App-Header">
        <img className='img-header' alt='Logo' src={require('./LostInTranslation_Resources/Logo.png')}></img>
        Lost In Translation

      </header>
      <Switch>
        <Route exact path='/'>
          <Redirect to='/login'/>
        </Route>
        <Route exact path='/login' component={ Login }/>
        <Route path='/translator' component={ Translator }/>
        <Route path='/profile' component={ Profile }/>
        <Route path='*' component={ PageNotFound }></Route>
      </Switch>
    </div>
    </Router>
  );
}

export default App;
